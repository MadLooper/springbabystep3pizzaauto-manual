package implementation;

/**
 * Created by KW on 10/11/2017.
 */
public class GoodPizza implements IPizza {
    private String name;
    private int price;

    public GoodPizza(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

}
