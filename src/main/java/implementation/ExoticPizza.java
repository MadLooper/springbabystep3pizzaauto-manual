package implementation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
/**
 * Created by KW on 10/11/2017.
 */
@Component
public class ExoticPizza implements IPizza {
    private String name;
    private int price;

    public ExoticPizza(
            @Value("Egzotyczna") String name,
            @Value("35") int price)
//  w konstruktorze przez Value wstrzykujemy wartosci -dla autoconfig
    {
        this.name = name;
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

}
