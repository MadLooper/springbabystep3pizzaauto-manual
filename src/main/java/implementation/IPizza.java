package implementation;

import org.springframework.stereotype.Component;

/**
 * Created by KW on 10/11/2017.
 */
@Component
public interface IPizza {
    int getPrice();
    String getName();

}
