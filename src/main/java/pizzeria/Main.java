package pizzeria;

import implementation.IOrder;
import implementation.Order;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import config.Autoconfig;

/**
 * Created by KW on 10/11/2017.
 */
public class Main {
    public static void main(String[] args) {
       // AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(config.class);
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Autoconfig.class);
    IOrder order = context.getBean(Order.class);
    order.printOrder();
    }
}
//Auto - deklarujemy pusta konfiguracje w autoconfig
//Auto - deklarujemy komponenty @Component
