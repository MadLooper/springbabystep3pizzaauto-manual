package config;

import org.springframework.context.annotation.*;

/**
 * Created by KW on 10/11/2017.
 */
@Configuration
@ComponentScan("implementation")
public class Autoconfig {
}
//tworzymy klase, ktora umozliwi skanowanie w celu wyszukiwania komponentow w obrebie pakietu
//w automatycznej konfiguracji nie deklarujemy beanow, tylko odnotacja
//w exotic pizza np. @Value